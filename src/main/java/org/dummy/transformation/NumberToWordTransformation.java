package org.dummy.transformation;

import java.util.Scanner;

/**
 * The class is responsible to transform any number bounded by the upper and lower bounds of java long type
 * to its English word representation
 * The number is accepted from a standard input and the resulted sentence is displayed in console
 * In case the input is not valid number, i.e. contains not numeric characters or the number exceeds
 * the bounds of long java type, the appropriate message is displayed to the user
 * In case the number is negative, 'Minus' word is added to the sentence.
 */
public class NumberToWordTransformation {


    /**
     * array that represents English word representation for the tens (10,20,30,40,50,60,70,80,90)
     */
    private static final String[] tenNames = {
            "",
            "ten ",
            "twenty ",
            "thirty ",
            "forty ",
            "fifty ",
            "sixty ",
            "seventy ",
            "eighty ",
            "ninety "
    };

    /**
     * array that represents English word representation for the numbers between 1-19
     */
    private static final String[] onesAndTeenNames = {
            "",
            "one ",
            "two ",
            "three ",
            "four ",
            "five ",
            "six ",
            "seven ",
            "eight ",
            "nine ",
            "ten ",
            "eleven ",
            "twelve ",
            "thirteen ",
            "fourteen ",
            "fifteen ",
            "sixteen ",
            "seventeen ",
            "eighteen ",
            "nineteen "
    };

    /**
     * array holds English word representation for multiple of thousands
     */
    private static final String[] multipleThousandsNames = {
            "",
            "thousand ",
            "million ",
            "billion ",
            "trillion ",
            "quadrillion ",
            "quintillion "
    };

    /**
     * Convert one or two digits number
     * This method handles number to word translation only for numbers less than one hundred
     * @param smallNumber Number to be translated to English word.
     * @return String that corresponds to a given number
     */
    private static String convertSmallNumber(int smallNumber){

        String result = "";

        if (smallNumber > 19)
            result += tenNames[smallNumber / 10] + onesAndTeenNames[smallNumber % 10];
        else
            result += onesAndTeenNames[smallNumber];

        return result;
    }


    /**
     * The method accepts a number and transforms it to english word representation
     * @param number Number to transform
     * @return String sentence corresponding to English word representation of a given number
     */
    public static String transform (long number){

        if (number == 0) { return "zero"; }

        StringBuilder output = new StringBuilder();

        // loop backward through multipleThousandsNames array and check if the number is divisible
        // by 1000 in the power of index
        // i.e. if the number is divisible by 10^18 , 10^15, 10^12....
        // if the number is divisible (quotient != 0) ,perform recursive call for  left side remaining digits
        // and append the corresponding word from the array
        for (int i=multipleThousandsNames.length - 1;i>0;i--){

            long divisible = (long) Math.pow(1000,i);  // 1000^6, 1000^5, 1000^4, 1000^3, 1000^2, 1000^1
            if ((number / divisible ) != 0) {
                output.append(transform(number / divisible ) + multipleThousandsNames[i]);
                number %= divisible;
            }
        }

        if ((number / 100) != 0) {
            output.append(transform(number / 100) + "hundred ");
            number %= 100;
        }

        if ((number) != 0) {
            if (output.length() > 0)
                output.append("and ");
            output.append(convertSmallNumber((int) Math.abs(number % 100)));
        }
        return output.toString();

    }

    /**
     * Replace first character to upper case, add minus if needed and remove extra 'ands'
     * @param word  unformatted word number representation
     * @param negativePrefix  indicates whether the number is negative or positive
     * @return Formatted word that represents number
     */
    public static String formattedWord(String word,boolean negativePrefix){

        // remove " and " words except the last one
        int index = word.lastIndexOf(" and ");
        if (index != -1)
            word = word.substring(0,index).replace(" and ", " ") + word.substring(index);

        // append "Minus" in case of negative number
        if (negativePrefix)
            word = "Minus " + word;
        else
        // replace the first letter to upper case
            word = word.substring(0,1).toUpperCase() + word.substring(1);

        return word.trim();
    }

 /**
  * Main method, called when the process is executed
  * @param args arguments provided to the program
 */
    public static void main (String[] args){

        long input;
        // accept a number from the standard input
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter number between -9,223,372,036,854,775,808 and 9,223,372,036,854,775,807 ");
        String inputLine = sc.nextLine();
        try {
            input = Long.parseLong(inputLine);
        } catch (NumberFormatException e) {
            System.out.println ("Given input: " + inputLine + " is not a valid number or out of the range");
            return;
        }
        boolean negativePrefix = (inputLine.substring(0,1).equals("-"));
        System.out.println("number: " + input + " | word: " + formattedWord(transform(input),negativePrefix));

    }
}

package org.dummy.transformation;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Junit class which is used to execute unit testing
 */
public class NumberToWordTransformationTests {

    /**
     * Test transformation logic
      */
    @Test
    public void testTransformation() {

        // test zero
        assertEquals("zero", NumberToWordTransformation.transform(0));
        // test single digit
        assertEquals("four", NumberToWordTransformation.transform(4).trim());
        // test two digits
        assertEquals("fifteen", NumberToWordTransformation.transform(15).trim());
        // test hundreds
        assertEquals("one hundred", NumberToWordTransformation.transform(100).trim());
        // test thousand with "and"
        assertEquals("two thousand and ten", NumberToWordTransformation.transform(2010).trim());
        // test negative number (minus prefix is not handled by transform method)
        assertEquals("one hundred and twenty", NumberToWordTransformation.transform(-120).trim());
        // test big number
        assertEquals("one hundred and twenty two billion three hundred and thirty three million four hundred and forty four thousand five hundred and fifty five", NumberToWordTransformation.transform(122333444555l).trim());
        // test upper bound
        assertEquals("nine quintillion two hundred and twenty three quadrillion three hundred and seventy two trillion thirty six billion eight hundred and fifty four million seven hundred and seventy five thousand eight hundred and seven", NumberToWordTransformation.transform(9223372036854775807l).trim());
        // test lower bound
        assertEquals("nine quintillion two hundred and twenty three quadrillion three hundred and seventy two trillion thirty six billion eight hundred and fifty four million seven hundred and seventy five thousand eight hundred and eight", NumberToWordTransformation.transform(-9223372036854775808l).trim());
    }

    /**
     * Test that first letter is replaced to upper case and only the last "and" remains (if any)
     * Test also adding "Minus" prefix in case of negative number
      */
    @Test
    public void testFormatting() {

        // test upper case for the first letter
        assertEquals("Twelve", NumberToWordTransformation.formattedWord("twelve",false));
        // test the sentence with one "and"
        assertEquals("Two thousand and ten", NumberToWordTransformation.formattedWord("two thousand and ten",false));
        // test the sentence with multiple "ands"
        assertEquals("One hundred twenty two billion three hundred thirty three million four hundred forty four thousand five hundred and fifty five",NumberToWordTransformation.formattedWord("one hundred and twenty two billion three hundred and thirty three million four hundred and forty four thousand five hundred and fifty five",false));
        // test adding minus word for negative number
        assertEquals("Minus one hundred",NumberToWordTransformation.formattedWord("one hundred",true));
    }

}

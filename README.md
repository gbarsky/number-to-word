## Description of the project

The program gets a number from standard input (console) and it gets validated. If it contains non-numeric characters or the number exceeds the bounds of long java type (-2^63 ....2^63-1), the appropriate message is displayed to the user.
Once the number is validated, it is passed to the transform method.

**Transform** method is a recusrive method that implements multiple IF conditions. In each of this condition, the number is divided by multiple of tens (100,1000,1000000 and so on).
If the resulting quotient is non-zero, it calls itself again with that quotient for converting these remaining digits to the word. 

For example, let's assume number is 525320. 
First, we try to divide the number by multiple of tens , starting with the biggest one (10^18 - quintillion)
The first one which produces non-zero quotient is division by 1000 (525320 / 1000 = 525). The method calls itself with 525 and when it returns it appends the word "thousand" because the division was by 1000. 
525 is then divisible by 100 to produce non-zero quotient (525/100 = 5). The method then calls itself again with 5 and when it returns it appends the word "hundred"
When method reaches the point when number is less than 100, it calls another method **convertSmallNumber** to translate the number based on the two static arrays.
One array contains the word representation of numbers between 1-19 and another contains word representaion of tens (ten, twenty....)

So, as a result, 525320 is converted into this sentence.

*five hundred twenty five thousand three hundred and twenty*

The last step in the program is formatting - to ensure that the sentence begins with upper letter and 'minus' word is added for negative number 

## Project metadata

The project was built and tested in Java 1.8

It is Maven project.

Maven version 3.3.9

JavaDoc are generated and can be found under *docs* folder

## Build a project

Clone this repo. 

In the project home directory run:

*mvn install* or 

*mvn package*

As a part of build process, Junit tests are also executed.

## How to run the program 

Run using java command line:

*java -cp target/numberToWord-1.0-SNAPSHOT.jar org.dummy.transformation.NumberToWordTransformation*

Run using maven:

*mvn exec:java*
